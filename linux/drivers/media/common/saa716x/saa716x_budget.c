#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/delay.h>
#include <linux/pci.h>
#include <linux/signal.h>
#include <linux/sched.h>
#include <linux/interrupt.h>
#include <linux/device.h>
#include <linux/spinlock.h>
#include <linux/io.h>
#include <linux/spi/spi.h>
#include <asm/io.h>
#include <asm/pgtable.h>
#include <asm/page.h>
#include <linux/kmod.h>
#include <linux/vmalloc.h>
#include <linux/init.h>
#include <linux/device.h>

#include <linux/i2c.h>

#include "dvbdev.h"
#include "dvb_demux.h"
#include "dmxdev.h"
#include "dvb_frontend.h"
#include "dvb_net.h"


#include "saa716x_mod.h"
#include "saa716x_msi_reg.h"
#include "saa716x_gpio_reg.h"
#include "saa716x_fgpi_reg.h"
#include "saa716x_dma_reg.h"
#include "saa716x_greg_reg.h"

#include "saa716x_adap.h"
#include "saa716x_gpio.h"
#include "saa716x_vip.h"
#include "saa716x_aip.h"
#include "saa716x_rom.h"
#include "saa716x_cgu.h"
#include "saa716x_boot.h"
#include "saa716x_i2c.h"
#include "saa716x_msi.h"
#include "saa716x_spi.h"
#include "saa716x_dma.h"
#include "saa716x_fgpi.h"
#include "saa716x_priv.h"


#include "saa716x_budget.h"
#include "mb86a16.h"
#include "stv6110x.h"
#include "ix2470.h"
#include "stv090x.h"

#include "cxd2099.h"

unsigned int verbose;
module_param(verbose, int, 0644);
MODULE_PARM_DESC(verbose, "verbose startup messages, default is 1 (yes)");

unsigned int int_type;
module_param(int_type, int, 0644);
MODULE_PARM_DESC(int_type, "force Interrupt Handler type: 0=INT-A, 1=MSI, 2=MSI-X. default INT-A mode");

//unsigned int dupx;
//module_param(dupx, int, 0644);
//MODULE_PARM_DESC(dupx, "Duplex xfer disable, default is 0 (no)");


#define DRIVER_NAME	"SAA716x Budget"

static int saa716x_budget_pci_probe(struct pci_dev *pdev, const struct pci_device_id *pci_id)
{
	struct saa716x_dev *saa716x;
	int err = 0;

	saa716x = kzalloc(sizeof (struct saa716x_dev), GFP_KERNEL);
	if (saa716x == NULL) {
		printk(KERN_ERR "saa716x_budget_pci_probe ERROR: out of memory\n");
		err = -ENOMEM;
		goto fail0;
	}

	saa716x->verbose	= verbose;
	saa716x->int_type	= int_type;
	saa716x->pdev		= pdev;
	saa716x->config		= (struct saa716x_config *) pci_id->driver_data;

	err = saa716x_pci_init(saa716x);
	if (err) {
		dprintk(SAA716x_ERROR, 1, "SAA716x PCI Initialization failed");
		goto fail1;
	}

	err = saa716x_cgu_init(saa716x);
	if (err) {
		dprintk(SAA716x_ERROR, 1, "SAA716x CGU Init failed");
		goto fail1;
	}

	err = saa716x_core_boot(saa716x);
	if (err) {
		dprintk(SAA716x_ERROR, 1, "SAA716x Core Boot failed");
		goto fail2;
	}
	dprintk(SAA716x_DEBUG, 1, "SAA716x Core Boot Success");

	err = saa716x_msi_init(saa716x);
	if (err) {
		dprintk(SAA716x_ERROR, 1, "SAA716x MSI Init failed");
		goto fail2;
	}

	err = saa716x_jetpack_init(saa716x);
	if (err) {
		dprintk(SAA716x_ERROR, 1, "SAA716x Jetpack core initialization failed");
		goto fail1;
	}

	err = saa716x_i2c_init(saa716x);
	if (err) {
		dprintk(SAA716x_ERROR, 1, "SAA716x I2C Initialization failed");
		goto fail3;
	}
	saa716x_gpio_init(saa716x);
	err = saa716x_dump_eeprom(saa716x); /* do not exit in case failed */
	if (err) {
		dprintk(SAA716x_ERROR, 1, "SAA716x EEPROM dump failed");
	}

	err = saa716x_eeprom_data(saa716x); /* do not exit in case failed */
	if (err) {
		dprintk(SAA716x_ERROR, 1, "SAA716x EEPROM read failed, trying to continue ..");
	}

	SAA716x_EPWR(GREG, GREG_VI_CTRL, 0x0689f14); /* */ //0x2c688f00); // 0x4688f5c // 0x0689f04
	SAA716x_EPWR(GREG, GREG_FGPI_CTRL, 0x291); /* FGPI3=P2, FGPI2=P1, FGPI1=P5, FGPI0=P4 */

	err = saa716x_dvb_init(saa716x);
	if (err) {
		dprintk(SAA716x_ERROR, 1, "SAA716x DVB initialization failed");
		goto fail4;
	}

	return 0;

fail4:
	saa716x_dvb_exit(saa716x);
fail3:
	saa716x_i2c_exit(saa716x);
fail2:
	saa716x_pci_exit(saa716x);
fail1:
	kfree(saa716x);
fail0:
	return err;
}

static void saa716x_budget_pci_remove(struct pci_dev *pdev)
{
	struct saa716x_dev *saa716x = pci_get_drvdata(pdev);

	saa716x_dvb_exit(saa716x);
	saa716x_i2c_exit(saa716x);
	saa716x_pci_exit(saa716x);
	kfree(saa716x);
}

static irqreturn_t saa716x_budget_pci_irq(int irq, void *dev_id)
{
	struct saa716x_dev *saa716x	= dev_id;
	struct saa716x_dvb *dvb0;
	struct saa716x_dvb *dvb1;
	struct saa716x_fgpi_stream_port *fgpi;
	struct saa716x_dmabuf *dmabuf;
	u32 reg, stat_h, stat_l, index;
//	unsigned long flags;

	if (unlikely(!saa716x)) {
		printk("%s: saa716x=NULL", __func__);
		return IRQ_NONE;
	}
//	spin_lock_irqsave(&saa716x->irq_lock, flags);
	stat_l = SAA716x_EPRD(MSI, MSI_INT_STATUS_L);
	stat_h = SAA716x_EPRD(MSI, MSI_INT_STATUS_H);

	/* check if it is ours */
	if (!stat_l && !stat_h)
		return IRQ_NONE;

	/* check for Hotplug events */
	if (stat_l == 0xffffffff && stat_h == 0xffffffff) {
		dprintk(SAA716x_ERROR, 1, "ERROR: Hardware unplugged");
		return IRQ_NONE;
	}

	/* acknowledge */
	SAA716x_EPWR(MSI, MSI_INT_STATUS_CLR_L, stat_l);
	SAA716x_EPWR(MSI, MSI_INT_STATUS_CLR_H, stat_h);
	dvb0 = &saa716x->dvb[0];
	dvb1 = &saa716x->dvb[1];

	/* debug for now */
	dprintk(SAA716x_DEBUG, 1, "MSI STAT L=<%02x> H=<%02x>", stat_l, stat_h);
	if (stat_l & MSI_INT_TAGACK_FGPI_3) { /* FGPI_0 */
		dprintk(SAA716x_DEBUG, 1, "DEBUG: FGPI TAG 3"); /* FIXME */
		fgpi = &saa716x->fgpi[3];
		dmabuf = fgpi->dma_buf;
		/* get buffer index */
		reg = SAA716x_EPRD(BAM, BAM_FGPI3_DMA_BUF_MODE);
		if (saa716x->revision) {
			/* clear source interrupt (only V1) */
			SAA716x_EPWR(FGPI3, INT_CLR_STATUS, 0x7f);
			/* set number of buffer to mode (only V1) */
			SAA716x_EPWR(FGPI3, BAM_FGPI3_DMA_BUF_MODE, (reg | (FGPI_BUFFERS - 1)));
		}
		if (reg == 0xffffffff)
			dprintk(SAA716x_ERROR, 1, "ERROR: Invalid Buffer Mode");
		index = GET_INDEX(reg);
		dprintk(SAA716x_DEBUG, 1, "Index:%d", index);
//		dvb_dmx_swfilter_packets(&dvb0->demux, dmabuf[index].mem_virt, 348);
	}
	if (stat_l & MSI_INT_TAGACK_FGPI_2) {
		dprintk(SAA716x_DEBUG, 1, "DEBUG: FGPI TAG 2"); /* FIXME */
		fgpi = &saa716x->fgpi[2];
		dmabuf = fgpi->dma_buf;
		/* get buffer index */
		reg = SAA716x_EPRD(BAM, BAM_FGPI2_DMA_BUF_MODE);
		if (!saa716x->revision) {
			/* clear source interrupt (only V1) */
			SAA716x_EPWR(FGPI2, INT_CLR_STATUS, 0x7f);
			/* set number of buffer to mode (only V1) */
			SAA716x_EPWR(FGPI2, BAM_FGPI2_DMA_BUF_MODE, (reg | (FGPI_BUFFERS - 1)));
		}
		if (reg == 0xffffffff)
			dprintk(SAA716x_ERROR, 1, "ERROR: Invalid Buffer Mode");
		index = GET_INDEX(reg);
		dprintk(SAA716x_DEBUG, 1, "Index:%d", index);
//		dvb_dmx_swfilter_packets(&dvb1->demux, dmabuf[index].mem_virt, 348);
	}
	if (stat_l & MSI_INT_TAGACK_FGPI_1) {
		dprintk(SAA716x_DEBUG, 1, "DEBUG: FGPI TAG 1");
		fgpi = &saa716x->fgpi[1];
		dmabuf = fgpi->dma_buf;
		/* get buffer index */
		reg = SAA716x_EPRD(BAM, BAM_FGPI1_DMA_BUF_MODE);
		if (!saa716x->revision) {
			/* clear source interrupt (only V1) */
			SAA716x_EPWR(FGPI1, INT_CLR_STATUS, 0x7f);
			/* set number of buffer to mode (only V1) */
			SAA716x_EPWR(FGPI1, BAM_FGPI1_DMA_BUF_MODE, (reg | (FGPI_BUFFERS - 1)));
		}
		if (reg == 0xffffffff)
			dprintk(SAA716x_ERROR, 1, "ERROR: Invalid Buffer Mode");
		index = GET_INDEX(reg);
		dprintk(SAA716x_DEBUG, 1, "Index:%d", index);
		dvb_dmx_swfilter_packets(&dvb0->demux, dmabuf[index].mem_virt, 348);
	}
	if (stat_l & MSI_INT_TAGACK_FGPI_0) {
		dprintk(SAA716x_DEBUG, 1, "DEBUG: FGPI TAG 0");
		fgpi = &saa716x->fgpi[0];
		dmabuf = fgpi->dma_buf;
		/* get buffer index */
		reg = SAA716x_EPRD(BAM, BAM_FGPI0_DMA_BUF_MODE);
		if (!saa716x->revision) {
			/* clear source interrupt (only V1) */
			SAA716x_EPWR(FGPI0, INT_CLR_STATUS, 0x7f);
			/* set number of buffer to mode (only V1) */
			SAA716x_EPWR(FGPI0, BAM_FGPI0_DMA_BUF_MODE, (reg | (FGPI_BUFFERS - 1)));
		}
		if (reg == 0xffffffff)
			dprintk(SAA716x_ERROR, 1, "ERROR: Invalid Buffer Mode");
		index = GET_INDEX(reg);
		dprintk(SAA716x_DEBUG, 1, "Index:%d", index);
		dvb_dmx_swfilter_packets(&dvb1->demux, dmabuf[index].mem_virt, 348);
	}
//	spin_unlock_irqrestore(&saa716x->irq_lock, flags);
	return IRQ_HANDLED;
}

static irqreturn_t saa716x_fgpi_handler(int irq, void *dev_id)
{
	struct saa716x_dev *saa716x = dev_id;
	u32 reg, stat_l, index, port, curr, prev, device; // i;
//	u8 *dbuf;
	struct saa716x_dvb *dvb = saa716x->dvb;

	if (unlikely(!saa716x)) {
		printk("%s: saa716x=NULL", __func__);
		return IRQ_NONE;
	}

	stat_l = SAA716x_EPRD(MSI, MSI_INT_STATUS_L);
	/* check if it is ours */
	if (!stat_l)
		return IRQ_NONE;

	/* check for Hotplug events */
	if (stat_l == 0xffffffff) {
		dprintk(SAA716x_ERROR, 1, "ERROR: Hardware unplugged");
		return IRQ_NONE;
	}
	port = ffs(((stat_l >> 6) & 0x0f)) - 1;
	BUG_ON(port < 0 || port > 3);
	SAA716x_EPWR(MSI, MSI_INT_STATUS_CLR_L, MSI_INT_TAGACK_FGPIx(port));

	reg = SAA716x_EPRD(BAM, BAM_FGPIx_DMA_BUF_MODE(port));
	if (!saa716x->revision) {
		SAA716x_EPWR(FGPIx(port), INT_CLR_STATUS, 0x7f);
		SAA716x_EPWR(FGPIx(port), BAM_FGPIx_DMA_BUF_MODE(port), (reg | (FGPI_BUFFERS - 1)));
	}
	if (reg == 0xffffffff)
		dprintk(SAA716x_ERROR, 1, "ERROR: Invalid Buffer Mode");

	index = GET_INDEX(reg);
	dprintk(SAA716x_DEBUG, 1, "MSI STAT L=<%02x> TAGACK FGPI-%d, get:%d", stat_l, port, index);

	if (port == 1 || port == 2)
		device = 1;
	if (port == 0 || port == 3)
		device = 0;

	dprintk(SAA716x_DEBUG, 1, "device:%d dupx:%d", device, dvb[device].dupx);
//	if (saa716x->dupx && (port == 3 || port == 2)) {
	if (dvb[device].dupx && (port == 3 || port == 2)) {
		/* Ignore */
		return IRQ_HANDLED;
	} else {
#if 0
		dbuf = saa716x->fgpi[port].dma_buf[index].mem_virt;
		for (i = 0; i < 348; i++) {
			if (!(i % 32) && !(i == 0))
				dprintk(SAA716x_DEBUG, 0, "\n    ");
			if (!(i % 24) || !(i % 16) || !(i % 8) || !(i % 4))
				dprintk(SAA716x_DEBUG, 0, "  ");
			if (i == 0)
				dprintk(SAA716x_DEBUG, 0, "    ");
			dprintk(SAA716x_DEBUG, 0, "%02x ", dbuf[i]);
		}
#endif
		dvb_dmx_swfilter_packets(&saa716x->dvb[device].demux,
					 saa716x->fgpi[port].dma_buf[index].mem_virt,
					 348);
	}
	return IRQ_HANDLED;
}

static int vp1028_dvbs_set_voltage(struct dvb_frontend *fe, fe_sec_voltage_t voltage)
{
	struct saa716x_dev *saa716x = fe->dvb->priv;

	switch (voltage) {
	case SEC_VOLTAGE_13:
		dprintk(SAA716x_ERROR, 1, "Polarization=[13V]");
		break;
	case SEC_VOLTAGE_18:
		dprintk(SAA716x_ERROR, 1, "Polarization=[18V]");
		break;
	case SEC_VOLTAGE_OFF:
		dprintk(SAA716x_ERROR, 1, "Frontend (dummy) POWERDOWN");
		break;
	default:
		dprintk(SAA716x_ERROR, 1, "Invalid = (%d)", (u32 ) voltage);
		return -EINVAL;
	}

	return 0;
}

struct mb86a16_config vp1028_mb86a16_config = {
	.demod_address	= 0x08,
	.set_voltage	= vp1028_dvbs_set_voltage,
};

static int saa716x_gpio_reset(struct saa716x_dev *saa716x, u32 gpio, int delay)
{
	saa716x_gpio_write(saa716x, gpio, 0);
	msleep(delay);
	saa716x_gpio_write(saa716x, gpio, 1);
	msleep(delay);
	return 0;
}

static int vp6018_lnb_power(struct dvb_frontend *fe, int pstate)
{
	struct dvb_adapter *dvb_adap	= fe->dvb;
	struct saa716x_dvb *dvb		= dvb_adap->priv;
	struct saa716x_dev *saa716x	= dvb->saa716x;

	dprintk(SAA716x_DEBUG, 1, "DEBUG: LNB Power state:%d", pstate);
	if (pstate == 1)
		saa716x_gpio_write(saa716x, 2, 1);
	else
		saa716x_gpio_write(saa716x, 2, 0);
	return 0;
}

static int vp6018_sns_power(struct dvb_frontend *fe, int *status)
{
	struct dvb_adapter *dvb_adap	= fe->dvb;
	struct saa716x_dvb *dvb		= dvb_adap->priv;
	struct saa716x_dev *saa716x	= dvb->saa716x;

	*status = saa716x_gpio_read(saa716x, 20);
	dprintk(SAA716x_DEBUG, 1, "DEBUG: LNB Power state:%d", *status);
	return 0;
}

static struct stv090x_config knc_dvbs2_twin_stv090x_config = {
	.device			= STV0900,
	.demod_mode		= STV090x_DUAL,
	.clk_mode		= STV090x_CLK_EXT,

	.xtal			= 27000000,
	.address		= (0xd0 >> 1),

	.ts1_mode		= STV090x_TSMODE_DVBCI,
	.ts2_mode		= STV090x_TSMODE_DVBCI,

	.ts1_tei		= 1,
	.ts2_tei		= 1,

	.ts1_clk		= 9000000,
	.ts2_clk		= 9000000,

	.fifo1_mode		= STV090x_TSFIFO_SMOOTHED,
	.fifo2_mode		= STV090x_TSFIFO_SMOOTHED,
	.sec			= {
		{
			.voltage	= 2,
		}, {
			.voltage	= 3,
		}
	},

	.repeater_level		= STV090x_RPTLEVEL_16,

	.adc1_range		= STV090x_ADC_1Vpp,
	.adc2_range		= STV090x_ADC_1Vpp,
};

static struct ix2470_cfg knc_dvbs2_twin_ix2470_config = {
	.name		= "BS2S7HZ6306",
	.addr		= (0xc0 >> 1),
	.step_size	= IX2470_STEP_1000,
	.bb_gain	= IX2470_GAIN_2dB,
	.t_lock		= 200,
};

struct stv090x_config vp6018_stv090x_cfg = {
	.device		= STV0903,
	.demod_mode	= STV090x_SINGLE,
	.clk_mode	= STV090x_CLK_EXT,

	.xtal		= 8000000,
	.address	= (0xd0 >> 1),

	.sec[0]		= {
	.voltage	= 1,
	.power		= vp6018_lnb_power,
	.sense		= vp6018_sns_power,
	},

//	.ts3_mode	= STV090x_TSMODE_SERIAL_PUNCTURED,
	.repeater_level	= STV090x_RPTLEVEL_16,
};

struct stv6110x_config vp6018_stv6110x_cfg = {
	.addr		= (0xc0 >> 1),
	.refclk		= 16000000,
	.clk_div	= 2,
};

#define STV090x_CFGUPDATE(__config, __ctl) {				\
	(__config)->tuner_init		= (__ctl)->tuner_init;		\
	(__config)->tuner_set_mode	= (__ctl)->tuner_set_mode;	\
	(__config)->tuner_set_frequency	= (__ctl)->tuner_set_frequency;	\
	(__config)->tuner_get_frequency	= (__ctl)->tuner_get_frequency;	\
	(__config)->tuner_set_bandwidth	= (__ctl)->tuner_set_bandwidth;	\
	(__config)->tuner_get_bandwidth	= (__ctl)->tuner_get_bandwidth;	\
	(__config)->tuner_set_bbgain	= (__ctl)->tuner_set_bbgain;	\
	(__config)->tuner_get_bbgain	= (__ctl)->tuner_get_bbgain;	\
	(__config)->tuner_set_refclk	= (__ctl)->tuner_set_refclk;	\
	(__config)->tuner_get_status	= (__ctl)->tuner_get_status;	\
}

struct cxd2099_cfg knc_dvbs2_twin_cxd2099_config[2] = {
	{
		.bitrate	= 62000,
		.adr		= 0x40,
		.polarity	= 0,
		.clock_mode	= 0,

	}, {
		.bitrate	= 62000,
		.adr		= 0x41,
		.polarity	= 0,
		.clock_mode	= 0,
	}
};

static int budget_frontend_attach(struct saa716x_dvb *dvb, int count)
{
	struct saa716x_dev *saa716x	= dvb->saa716x;
	struct pci_dev *pdev		= saa716x->pdev;
	u32 subdev			= pdev->subsystem_device;
	struct saa716x_i2c *i2c		= saa716x->i2c;
	struct saa716x_i2c *i2c_a	= &i2c[SAA716x_I2C_BUS_A];
	struct saa716x_i2c *i2c_b	= &i2c[SAA716x_I2C_BUS_B];

	struct stv6110x_devctl *ctl;

	switch (subdev) {
	case TWINHAN_VP_1028:
		/* VP-1028 */
		/* GPIO  7: (INP) IR */
		/* GPIO 10: (OUT) ST1 OFF */
		/* GPIO 12: (OUT) ST1 RST */
		/* GPIO 20: (OUT) LNB */
		/* GPIO 22: (OUT) POL */
		break;
	case TWINHAN_VP_3071:
		/* VP-3071 */
		break;
	case TWINHAN_VP_6002:
		/* VP-6002 */
		break;
	case TWINHAN_VP_6018:
		/* single frontend */
		if (count == 1)
			break;

		dprintk(SAA716x_DEBUG, 1, "Adapter (%d) SAA716x frontend Init", count);
		dprintk(SAA716x_DEBUG, 1, "Adapter (%d) Device ID=%02x", count, subdev);
		/* VP-6018 */
		/* GPIO  2: (OUT) LNB ON/OFF */
		/* GPIO  3: (INP) CI Boot */
		/* GPIO  4: (OUT) Demod RESET */
		/* GPIO  5: (INP) CAM Detect */
		/* GPIO  6: (INP) IR */
		/* GPIO 20: (INP) PRT */
		/* GPIO 26: (OUT) SPI NSS */

		/* setup GPIO configuration */
		saa716x_gpio_set_output(saa716x, 2);
		saa716x_gpio_set_input(saa716x, 3);
		saa716x_gpio_set_output(saa716x, 4);
		saa716x_gpio_set_input(saa716x, 5);
		saa716x_gpio_set_input(saa716x, 6);
		saa716x_gpio_set_input(saa716x, 20);
		saa716x_gpio_set_output(saa716x, 26);

		/* Reset 903 */
		saa716x_gpio_reset(saa716x, 4, 250);

		/* attach frontend */
		dvb->fe = dvb_attach(stv090x_attach, &vp6018_stv090x_cfg, &i2c_a->i2c_adapter, STV090x_DEMODULATOR_0);
		if (!dvb->fe) {
			dprintk(SAA716x_ERROR, 1, "ERROR: STV090x not found");
			goto err;
		}
		ctl = dvb_attach(stv6110x_attach, dvb->fe, &vp6018_stv6110x_cfg, &i2c_a->i2c_adapter);
		if (!ctl) {
			dprintk(SAA716x_ERROR, 1, "ERROR: STV6110x not found");
			goto err;
		}
		STV090x_CFGUPDATE(&vp6018_stv090x_cfg, ctl);
		/* Initialize tuner output clock */
		if (dvb->fe->ops.init)
			dvb->fe->ops.init(dvb->fe);
		break;
	case KNC_Dual_S2:
		break;
	case KNC_DVBS2_Twin:
		if (count == 0) {
			/* DVB_S2 Twin */
			dprintk(SAA716x_DEBUG, 1, "Adapter(%d) SAA716x frontend init", count);
			dprintk(SAA716x_DEBUG, 1, "Adapter(%d) Device ID=%02x", count, subdev);

			/* STV0900 */
			/* GPIO 2: (OUT) Polarization (13/18v) Tuner 1 */
			/* GPIO 3: (OUT) Polarization (13/18v) Tuner 2 */

			/* SAA7160 */
			/* GPIO  0: STV0900 Reset */
			saa716x_gpio_set_output(saa716x, 0);
			dprintk(SAA716x_DEBUG, 1, "Reset Demodulator..");
			saa716x_gpio_reset(saa716x, 0, 250); /* Reset STV0900 */

			/* GPIO  3: switch Tuner:1/2 to CI:1 */
			saa716x_gpio_set_output(saa716x, 3);

			/* GPIO  4: switch Tuner:1/2 to CI:2 */
			saa716x_gpio_set_output(saa716x, 4);

			/* GPIO  7: connected to CI:1 */
			/* GPIO  8: connected to CI:1 */
			/* GPIO  9: connected to CI:1 */

			/* GPIO 10: connected to CI:2 */
			/* GPIO 11: connected to CI:2 */
			/* GPIO 12: connected to CI:2 */

			/* GPIO 20: 5v for CI */
			saa716x_gpio_set_output(saa716x, 20);
			saa716x_gpio_write(saa716x, 20, 1);
			msleep(100);

			/* GPIO 22: CI Power Enable */
			saa716x_gpio_set_output(saa716x, 22);
			saa716x_gpio_write(saa716x, 22, 1);
			msleep(100);

			/* GPIO 23: CI Data Open (switch to CI:1/2) */
			saa716x_gpio_set_output(saa716x, 23);

			/* GPIO 19: 12v detection input */
			saa716x_gpio_set_input(saa716x, 19);
		}
		if (count == 0 || count == 1) {
			dvb->fe = dvb_attach(stv090x_attach,
					     &knc_dvbs2_twin_stv090x_config,
					     &i2c_b->i2c_adapter,
					     STV090x_DEMODULATOR_0 + count);

			if (dvb->fe) {
				struct ix2470_devctl *ctl;
				ctl = dvb_attach(ix2470_attach,
						 dvb->fe,
						 &knc_dvbs2_twin_ix2470_config,
						 &i2c_b->i2c_adapter);

				knc_dvbs2_twin_stv090x_config.tuner_set_frequency = ctl->tuner_set_frequency;
				knc_dvbs2_twin_stv090x_config.tuner_set_bandwidth = ctl->tuner_set_bandwidth;
				knc_dvbs2_twin_stv090x_config.tuner_get_status	  = ctl->tuner_get_status;

				saa716x_gpio_write(saa716x, 23, 0); // en data // was from CI attach
			}
		}
		break;
	}
	return 0;
err:
	dprintk(SAA716x_ERROR, 1, "ERROR: Frontend:%d attach failed", count);
	return -ENODEV;
}

static int knc_dvbs2twin_slot_ts_enable(struct dvb_ca_en50221 *ca, int slot)
{
	struct saa716x_dvb *dvb 	= ca->parent;
	struct saa716x_dev *saa716x 	= dvb->saa716x;

	int ret = 0;

	dprintk(SAA716x_DEBUG, 1, "Enabling TS through CI:%d", slot);
	dvb->dupx = 1;
	if (dvb->slot_ts_enable)
		ret = dvb->slot_ts_enable(ca, slot);

	return ret;
}

static int knc_dvbs2twinslot_shutdown(struct dvb_ca_en50221 *ca, int slot)
{
	struct saa716x_dvb *dvb 	= ca->parent;
	struct saa716x_dev *saa716x 	= dvb->saa716x;

	int ret = 0;

	dprintk(SAA716x_DEBUG, 1, "CI:%d shutdown", slot);
	dvb->dupx = 0;
	if (dvb->slot_shutdown)
		ret = dvb->slot_shutdown(ca, slot);

	return ret;
}

static int knc_dvbs2strmux_pos(struct saa716x_dvb *dvb, int *pos)
{
	struct saa716x_dev *saa716x 	= dvb->saa716x;

	if (!dvb->dupx) {
		if (dvb->count == 0)
			*pos = 3;
		if (dvb->count == 1)
			*pos = 2;
	} else {
		if (dvb->count == 0)
			*pos = 0;
		if (dvb->count == 1)
			*pos = 1;
	}
	dprintk(SAA716x_DEBUG, 1, "STREAM:%d", *pos);
	return 0;
}

static int budget_ci_attach(struct saa716x_dvb *dvb, int count)
{
	struct saa716x_dev *saa716x	= dvb->saa716x;
	struct pci_dev *pdev		= saa716x->pdev;
	u32 subdev			= pdev->subsystem_device;
	struct saa716x_i2c *i2c		= saa716x->i2c;
	struct saa716x_i2c *i2c_a	= &i2c[SAA716x_I2C_BUS_A];

	dprintk(SAA716x_DEBUG, 1, "trying to attach CI:%d", count);
	switch (subdev) {
	case KNC_DVBS2_Twin:
		saa716x->config->strmux_pos = knc_dvbs2strmux_pos;

		if (count == 0) {
			saa716x_gpio_set_output(saa716x, 8); /* Addr select */
			saa716x_gpio_write(saa716x, 8, 0); /* Addr: 0x80 */

			saa716x_gpio_set_output(saa716x, 7); /* CXD RESET */
			saa716x_gpio_set_output(saa716x, 9); /* Interrupt */
			saa716x_gpio_write(saa716x,  3, 0); // CI:1 - Tuner:1
			saa716x_gpio_write(saa716x, 7, 1);
			msleep(100);
			saa716x_gpio_write(saa716x, 7, 0); // Reset
			msleep(400);

			dvb->ci = dvb_attach(cxd2099_attach,
					     &knc_dvbs2_twin_cxd2099_config[0],
					     NULL,
					     &i2c_a->i2c_adapter);
			if (!dvb->ci) {
				dprintk(SAA716x_ERROR, 1, "ERROR: CXD2099 not found");
				goto err;
			}
			dvb->slot_ts_enable = dvb->ci->slot_ts_enable;
			dvb->slot_shutdown = dvb->ci->slot_shutdown;

			saa716x_gpio_write(saa716x, 23, 0); // en data

			dvb->ci->slot_ts_enable = knc_dvbs2twin_slot_ts_enable;
			dvb->ci->slot_shutdown	= knc_dvbs2twinslot_shutdown;
			dvb->ci->parent = dvb;
		}
		if (count == 1) {
#if 0
			saa716x_gpio_set_output(saa716x, 11);
			saa716x_gpio_write(saa716x, 11, 1);
			saa716x_gpio_set_output(saa716x, 10);
			saa716x_gpio_set_output(saa716x, 12);
			saa716x_gpio_write(saa716x, 10, 1);
			msleep(100);
			saa716x_gpio_write(saa716x, 10, 0);
			msleep(400);

			adapter->ci = dvb_attach(cxd2099_attach,
						 &knc_dvbs2_twin_cxd2099_config[1],
						 NULL,
						 &i2c_a->i2c_adapter);
			if (!adapter->ci)
				dprintk(SAA716x_ERROR, 1, "ERROR: CXD2099 not found");
#endif

		}
//		saa716x_gpio_write(saa716x, 23, 0);
#if 0
		saa716x_gpio_write(saa716x,  3, 0); // CI:1 - Tuner:1
		saa716x_gpio_write(saa716x,  4, 1); // CI:2 - Tuner:2
#endif
#if 1
		saa716x_gpio_write(saa716x,  4, 1); // CI:2 - Tuner:2
#endif
		break;
	}
	return 0;
err:
	dprintk(SAA716x_ERROR, 1, "ERROR: CI:%d attach failed", count);
	return -ENODEV;
}

#define SAA716x_MODEL_TWINHAN_VP6002	"Twinhan/Azurewave VP-6002"
#define SAA716x_DEV_TWINHAN_VP6002	"DVB-S"

#define SAA716x_MODEL_KNC1_DUALS2	"KNC One Dual S2"
#define SAA716x_DEV_KNC1_DUALS2		"1xDVB-S + 1xDVB-S/S2"

#define SAA716x_MODEL_KNC1_DVBS2_TWIN	"KNC One DVB-S2 Twin"
#define SAA716x_DEV_KNC1_DVBS2_TWIN	"DVB-S/S2 Dual CI"

#define SAA716x_MODEL_TWINHAN_VP6018	"Azurewave VP-6018"
#define SAA716x_DEV_TWINHAN_VP6018	"DVB-S/S2 CI"

#define SAA716x_MODEL_TWINHAN_VP3071	"Twinhan/Azurewave VP-3071"
#define SAA716x_DEV_TWINHAN_VP3071	"2x DVB-T"

#define SAA716x_MODEL_TWINHAN_VP1028	"Twinhan/Azurewave VP-1028"
#define SAA716x_DEV_TWINHAN_VP1028	"DVB-S"

static struct saa716x_config saa716x_vp3071_config = {
	.model_name		= SAA716x_MODEL_TWINHAN_VP3071,
	.dev_type		= SAA716x_DEV_TWINHAN_VP3071,
	.boot_mode		= SAA716x_EXT_BOOT,
	.fe_devs		= 2,
	.st_devs		= 2,
	.frontend_attach	= budget_frontend_attach,
	.irq_handler		= saa716x_budget_pci_irq,
	.i2c_rate		= SAA716x_I2C_RATE_100,
};

static struct saa716x_config saa716x_vp1028_config = {
	.model_name		= SAA716x_MODEL_TWINHAN_VP1028,
	.dev_type		= SAA716x_DEV_TWINHAN_VP1028,
	.boot_mode		= SAA716x_EXT_BOOT,
	.fe_devs		= 1,
	.st_devs		= 1,
	.frontend_attach	= budget_frontend_attach,
	.irq_handler		= saa716x_budget_pci_irq,
	.i2c_rate		= SAA716x_I2C_RATE_100,
};

static struct saa716x_config saa716x_vp6002_config = {
	.model_name		= SAA716x_MODEL_TWINHAN_VP6002,
	.dev_type		= SAA716x_DEV_TWINHAN_VP6002,
	.boot_mode		= SAA716x_EXT_BOOT,
	.fe_devs		= 1,
	.st_devs		= 1,
	.frontend_attach	= budget_frontend_attach,
	.irq_handler		= saa716x_budget_pci_irq,
	.i2c_rate		= SAA716x_I2C_RATE_100,
};

static struct saa716x_config saa716x_knc1_duals2_config = {
	.model_name		= SAA716x_MODEL_KNC1_DUALS2,
	.dev_type		= SAA716x_DEV_KNC1_DUALS2,
	.boot_mode		= SAA716x_EXT_BOOT,
	.fe_devs		= 2,
	.st_devs		= 4,
	.frontend_attach	= budget_frontend_attach,
	.irq_handler		= saa716x_budget_pci_irq,
	.i2c_rate		= SAA716x_I2C_RATE_100,
};

static struct saa716x_config saa716x_knc1_dvbs2_twin_config = {
	.model_name		= SAA716x_MODEL_KNC1_DVBS2_TWIN,
	.dev_type		= SAA716x_DEV_KNC1_DVBS2_TWIN,
	.boot_mode		= SAA716x_EXT_BOOT,
	.fe_devs		= 2,
	.st_devs		= 4,
	.frontend_attach	= budget_frontend_attach,
	.ci_attach		= budget_ci_attach,
	.irq_handler		= saa716x_fgpi_handler,
	.i2c_rate		= SAA716x_I2C_RATE_100,

	/*
	 * st_devs / fe_devs =  no. of MUX positions
	 * we just need to tag the MUX position alone
	 */
	.stream_cfg		= {
		{
			.ts_port = 3, /* FTA 0 */
			.mux_pos = 0,
		},{
			.ts_port = 2, /* FTA 1 */
			.mux_pos = 0,
		},{
			.ts_port = 0, /* CI 0 */
			.mux_pos = 1,
		},{
			.ts_port = 1, /* CI 1 */
			.mux_pos = 1,
		}
	}
};

static struct saa716x_config saa716x_vp6018_config = {
	.model_name		= SAA716x_MODEL_TWINHAN_VP6018,
	.dev_type		= SAA716x_DEV_TWINHAN_VP6018,
	.boot_mode		= SAA716x_EXT_BOOT,
	.fe_devs		= 1,
	.st_devs		= 1,
	.frontend_attach	= budget_frontend_attach,
	.irq_handler		= saa716x_budget_pci_irq,
	.i2c_rate		= SAA716x_I2C_RATE_100,
};

static struct pci_device_id saa716x_budget_pci_table[] = {

	MAKE_ENTRY(TWINHAN_TECHNOLOGIES, TWINHAN_VP_1028, SAA7160, &saa716x_vp1028_config), /* VP-1028 */
	MAKE_ENTRY(TWINHAN_TECHNOLOGIES, TWINHAN_VP_3071, SAA7160, &saa716x_vp3071_config), /* VP-3071 */
	MAKE_ENTRY(TWINHAN_TECHNOLOGIES, TWINHAN_VP_6002, SAA7160, &saa716x_vp6002_config), /* VP-6002 */
	MAKE_ENTRY(TWINHAN_TECHNOLOGIES, TWINHAN_VP_6018, SAA7160, &saa716x_vp6018_config), /* VP-6018 */
	MAKE_ENTRY(KNC_One,		 KNC_Dual_S2,	  SAA7160, &saa716x_knc1_duals2_config),
	MAKE_ENTRY(KNC_One,		 KNC_DVBS2_Twin,  SAA7160, &saa716x_knc1_dvbs2_twin_config),
	{ }
};
MODULE_DEVICE_TABLE(pci, saa716x_budget_pci_table);

static struct pci_driver saa716x_budget_pci_driver = {
	.name			= DRIVER_NAME,
	.id_table		= saa716x_budget_pci_table,
	.probe			= saa716x_budget_pci_probe,
	.remove			= saa716x_budget_pci_remove,
};

static int saa716x_budget_init(void)
{
	return pci_register_driver(&saa716x_budget_pci_driver);
}

static void saa716x_budget_exit(void)
{
	return pci_unregister_driver(&saa716x_budget_pci_driver);
}

module_init(saa716x_budget_init);
module_exit(saa716x_budget_exit);

MODULE_DESCRIPTION("SAA716x Budget driver");
MODULE_AUTHOR("Manu Abraham");
MODULE_LICENSE("GPL");
