#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/pci.h>
#include <linux/signal.h>
#include <linux/sched.h>
#include <linux/interrupt.h>
#include <linux/scatterlist.h>
#include <asm/page.h>
#include <asm/pgtable.h>

#include <linux/i2c.h>

#include "dvbdev.h"
#include "dvb_demux.h"
#include "dmxdev.h"
#include "dvb_frontend.h"
#include "dvb_net.h"

#include "saa716x_mod.h"

#include "saa716x_fgpi_reg.h"
#include "saa716x_dma_reg.h"
#include "saa716x_msi_reg.h"

#include "saa716x_cgu.h"
#include "saa716x_boot.h"
#include "saa716x_i2c.h"
#include "saa716x_msi.h"
#include "saa716x_spi.h"
#include "saa716x_dma.h"
#include "saa716x_fgpi.h"
#include "saa716x_priv.h"

void saa716x_fgpiint_disable(struct saa716x_dmabuf *dmabuf, int port)
{
	struct saa716x_dev *saa716x = dmabuf->saa716x;

	SAA716x_EPWR(FGPI(port), INT_ENABLE, 0); /* disable FGPI IRQ */
	SAA716x_EPWR(FGPI(port), INT_CLR_STATUS, 0x7f); /* clear status */
}
EXPORT_SYMBOL_GPL(saa716x_fgpiint_disable);

static u32 saa716x_init_ptables(struct saa716x_dmabuf *dmabuf, int port)
{
	struct saa716x_dev *saa716x = dmabuf->saa716x;

	u32 i;

	for (i = 0; i < FGPI_BUFFERS; i++)
		BUG_ON((dmabuf[i].mem_ptab_phys == 0));

	SAA716x_EPWR(MMU, MMU_DMACONFIG(port), (FGPI_BUFFERS - 1));

	SAA716x_EPWR(MMU, MMU_PTA0_LSB(port), PTA_LSB(dmabuf[0].mem_ptab_phys)); /* Low */
	SAA716x_EPWR(MMU, MMU_PTA0_MSB(port), PTA_MSB(dmabuf[0].mem_ptab_phys)); /* High */
	SAA716x_EPWR(MMU, MMU_PTA1_LSB(port), PTA_LSB(dmabuf[1].mem_ptab_phys)); /* Low */
	SAA716x_EPWR(MMU, MMU_PTA1_MSB(port), PTA_MSB(dmabuf[1].mem_ptab_phys)); /* High */
	SAA716x_EPWR(MMU, MMU_PTA2_LSB(port), PTA_LSB(dmabuf[2].mem_ptab_phys)); /* Low */
	SAA716x_EPWR(MMU, MMU_PTA2_MSB(port), PTA_MSB(dmabuf[2].mem_ptab_phys)); /* High */
	SAA716x_EPWR(MMU, MMU_PTA3_LSB(port), PTA_LSB(dmabuf[3].mem_ptab_phys)); /* Low */
	SAA716x_EPWR(MMU, MMU_PTA3_MSB(port), PTA_MSB(dmabuf[3].mem_ptab_phys)); /* High */
	SAA716x_EPWR(MMU, MMU_PTA4_LSB(port), PTA_LSB(dmabuf[4].mem_ptab_phys)); /* Low */
	SAA716x_EPWR(MMU, MMU_PTA4_MSB(port), PTA_MSB(dmabuf[4].mem_ptab_phys)); /* High */
	SAA716x_EPWR(MMU, MMU_PTA5_LSB(port), PTA_LSB(dmabuf[5].mem_ptab_phys)); /* Low */
	SAA716x_EPWR(MMU, MMU_PTA5_MSB(port), PTA_MSB(dmabuf[5].mem_ptab_phys)); /* High */
	SAA716x_EPWR(MMU, MMU_PTA6_LSB(port), PTA_LSB(dmabuf[6].mem_ptab_phys)); /* Low */
	SAA716x_EPWR(MMU, MMU_PTA6_MSB(port), PTA_MSB(dmabuf[6].mem_ptab_phys)); /* High */
	SAA716x_EPWR(MMU, MMU_PTA7_LSB(port), PTA_LSB(dmabuf[7].mem_ptab_phys)); /* Low */
	SAA716x_EPWR(MMU, MMU_PTA7_MSB(port), PTA_MSB(dmabuf[7].mem_ptab_phys)); /* High */

	return 0;
}

int saa716x_fgpi_setparams(struct saa716x_dmabuf *dmabuf,
			   struct fgpi_stream_params *stream_params,
			   int port)
{
	struct saa716x_dev *saa716x = dmabuf->saa716x;

	u32 ch, val, mid;
	u32 D1_XY_END, offst_1, offst_2;
	int i = 0;

	ch = saa716x->fgpi[port].dma_channel;

	/* Reset FGPI block */
	SAA716x_EPWR(FGPI(port), FGPI_SOFT_RESET, FGPI_SOFTWARE_RESET);

	/* Reset DMA channel */
	SAA716x_EPWR(BAM, BAM_FGPI_BUFMODE(port), 0x00000040);
	saa716x_init_ptables(dmabuf, ch);


	/* monitor BAM reset */
	val = SAA716x_EPRD(BAM, BAM_FGPI_BUFMODE(port));
	while (val && (i < 100)) {
		msleep(30);
		val = SAA716x_EPRD(BAM, BAM_FGPI_BUFMODE(port));
		i++;
	}

	if (val) {
		dprintk(SAA716x_ERROR, 1, "Error: BAM FGPI Reset failed!");
		return -EIO;
	}

	/* set buffer count */
	SAA716x_EPWR(BAM, BAM_FGPI_BUFMODE(port), FGPI_BUFFERS - 1);

	/* initialize all available address offsets */
	SAA716x_EPWR(BAM, BAM_FGPI_ADDR_OFFST_0(port), 0x0);
	SAA716x_EPWR(BAM, BAM_FGPI_ADDR_OFFST_1(port), 0x0);
	SAA716x_EPWR(BAM, BAM_FGPI_ADDR_OFFST_2(port), 0x0);
	SAA716x_EPWR(BAM, BAM_FGPI_ADDR_OFFST_3(port), 0x0);
	SAA716x_EPWR(BAM, BAM_FGPI_ADDR_OFFST_4(port), 0x0);
	SAA716x_EPWR(BAM, BAM_FGPI_ADDR_OFFST_5(port), 0x0);
	SAA716x_EPWR(BAM, BAM_FGPI_ADDR_OFFST_6(port), 0x0);
	SAA716x_EPWR(BAM, BAM_FGPI_ADDR_OFFST_7(port), 0x0);

	/* get module ID */
	mid = SAA716x_EPRD(FGPI(port), FGPI_MODULE_ID);
	if (mid != 0x14b0100)
		dprintk(SAA716x_ERROR, 1, "FGPI Id<%04x> is not supported", mid);

	/* Initialize FGPI block */
	SAA716x_EPWR(FGPI(port), FGPI_REC_SIZE, stream_params->samples * (stream_params->bits / 8));
	SAA716x_EPWR(FGPI(port), FGPI_STRIDE, stream_params->pitch);

	offst_1 = 0;
	offst_2 = 0;
	switch (stream_params->stream_type) {
	case FGPI_TRANSPORT_STREAM:
		dprintk(SAA716x_DEBUG, 1, "FGPI-%d Setup for TS", port);
		SAA716x_EPWR(FGPI(port), FGPI_CONTROL, 0x00000080);
		SAA716x_EPWR(FGPI(port), FGPI_SIZE, stream_params->lines);
		break;

	case FGPI_PROGRAM_STREAM:
		SAA716x_EPWR(FGPI(port), FGPI_CONTROL, 0x00000088);
		SAA716x_EPWR(FGPI(port), FGPI_SIZE, stream_params->lines);
		break;

	case FGPI_VIDEO_STREAM:
		SAA716x_EPWR(FGPI(port), FGPI_CONTROL, 0x00000088);
		SAA716x_EPWR(FGPI(port), FGPI_D1_XY_START, 0x00000002);

		if ((stream_params->stream_flags & FGPI_INTERLACED) &&
		    (stream_params->stream_flags & FGPI_ODD_FIELD) &&
		    (stream_params->stream_flags & FGPI_EVEN_FIELD)) {

			SAA716x_EPWR(FGPI(port), FGPI_SIZE, stream_params->lines / 2);
			SAA716x_EPWR(FGPI(port), FGPI_STRIDE, 768 * 4); /* interlaced stride of 2 lines */

			D1_XY_END  = (stream_params->samples << 16);
			D1_XY_END |= (stream_params->lines / 2) + 2;

			if (stream_params->stream_flags & FGPI_PAL)
				offst_1 = 768 * 2;
			else
				offst_2 = 768 * 2;

		} else {
			SAA716x_EPWR(FGPI(port), FGPI_SIZE, stream_params->lines);
			SAA716x_EPWR(FGPI(port), FGPI_STRIDE, 768 * 2); /* stride of 1 line */

			D1_XY_END  = stream_params->samples << 16;
			D1_XY_END |= stream_params->lines + 2;
		}

		SAA716x_EPWR(FGPI(port), FGPI_D1_XY_END, D1_XY_END);
		break;

	default:
		SAA716x_EPWR(FGPI(port), FGPI_CONTROL, 0x00000080);
		break;
	}

	SAA716x_EPWR(FGPI(port), FGPI_BASE_1, (ch << 21) + offst_1);
	SAA716x_EPWR(FGPI(port), FGPI_BASE_2, (ch << 21) + offst_2);

	return 0;
}

int saa716x_fgpi_start(struct saa716x_dev *saa716x, int port,
		       struct fgpi_stream_params *stream_params)
{
	u32 ch;
	u32 val;
	u32 i;
	int ret;

	ch = saa716x->fgpi[port].dma_channel;
	dprintk(SAA716x_ERROR, 1, "FGPI-%d Start DMA-%d", port, ch);

	SAA716x_EPWR(FGPI(port), FGPI_INTERFACE, 0);
	msleep(10);

	ret = saa716x_fgpi_setparams(saa716x->fgpi[port].dma_buf, stream_params, port);
	if (ret) {
		dprintk(SAA716x_ERROR, 1, "FGPI setup failed, ret=%d", ret);
		return -EIO;
	}

	val = SAA716x_EPRD(MMU, MMU_DMACONFIG(ch));
	SAA716x_EPWR(MMU, MMU_DMACONFIG(ch), val & ~0x40);
	SAA716x_EPWR(MMU, MMU_DMACONFIG(ch), val | 0x40);

	SAA716x_EPWR(FGPI(port), INT_ENABLE, 0x7F);

	val = SAA716x_EPRD(MMU, MMU_DMACONFIG(ch));
	i = 0;
	while (i < 500) {
		if (val & 0x80)
			break;
		msleep(10);
		val = SAA716x_EPRD(MMU, MMU_DMACONFIG(ch));
		i++;
	}

	if (!(val & 0x80)) {
		dprintk(SAA716x_ERROR, 1, "Error: PTE pre-fetch failed!");
		return -EIO;
	}

	val = SAA716x_EPRD(FGPI(port), FGPI_CONTROL);
	val |= 0x3000;

	saa716x_set_clk_external(saa716x, ch);

	SAA716x_EPWR(FGPI(port), FGPI_CONTROL, val);

	SAA716x_EPWR(MSI, MSI_INT_ENA_SET_L, MSI_TAGACK_FGPI(port));
#if 0
	SAA716x_EPWR(MSI, MSI_INT_ENA_SET_L, msi_int_ovrflw[port]);
	SAA716x_EPWR(MSI, MSI_INT_ENA_SET_L, msi_int_avint[port]);
#endif
	return 0;
}

int saa716x_fgpi_stop(struct saa716x_dev *saa716x, int port)
{
	u32 val;

	dprintk(SAA716x_ERROR, 1, "FGPI-%d Stop", port);

	SAA716x_EPWR(MSI, MSI_INT_ENA_CLR_L, MSI_TAGACK_FGPI(port));
#if 0
	SAA716x_EPWR(MSI, MSI_INT_ENA_CLR_L, msi_int_ovrflw[port]);
	SAA716x_EPWR(MSI, MSI_INT_ENA_CLR_L, msi_int_avint[port]);
#endif

	val = SAA716x_EPRD(FGPI(port), FGPI_CONTROL);
	val &= ~0x3000;
	SAA716x_EPWR(FGPI(port), FGPI_CONTROL, val);

	saa716x_set_clk_internal(saa716x, saa716x->fgpi[port].dma_channel);

	return 0;
}

int saa716x_fgpi_init(struct saa716x_dev *saa716x, int port)
{
	int i;
	int ret;

	dprintk(SAA716x_DEBUG, 1, "FGPI-%d initializing..", port);
	saa716x->fgpi[port].dma_channel = port + 6;
	for (i = 0; i < FGPI_BUFFERS; i++)
	{
		/* TODO: what is a good size for TS DMA buffer? */
		ret = saa716x_dmabuf_alloc(saa716x, &saa716x->fgpi[port].dma_buf[i], 20 * SAA716x_PAGE_SIZE);
		if (ret < 0) {
			return ret;
		}
	}

	return 0;
}

int saa716x_fgpi_exit(struct saa716x_dev *saa716x, int port)
{
	int i;

	for (i = 0; i < FGPI_BUFFERS; i++)
	{
		saa716x_dmabuf_free(saa716x, &saa716x->fgpi[port].dma_buf[i]);
	}

	return 0;
}
