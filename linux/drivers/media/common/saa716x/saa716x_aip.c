#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/pci.h>
#include <linux/signal.h>
#include <linux/sched.h>
#include <linux/interrupt.h>
#include <linux/scatterlist.h>
#include <asm/page.h>
#include <asm/pgtable.h>

#include <linux/i2c.h>

#include "dvbdev.h"
#include "dvb_demux.h"
#include "dmxdev.h"
#include "dvb_frontend.h"
#include "dvb_net.h"

#include "saa716x_mod.h"

#include "saa716x_i2c_reg.h"
#include "saa716x_msi_reg.h"
#include "saa716x_cgu_reg.h"
#include "saa716x_aip_reg.h"

#include "saa716x_cgu.h"
#include "saa716x_boot.h"
#include "saa716x_i2c.h"
#include "saa716x_msi.h"
#include "saa716x_spi.h"
#include "saa716x_dma.h"
#include "saa716x_fgpi.h"
#include "saa716x_priv.h"

int saa716x_aip_status(struct saa716x_dev *saa716x, u32 dev)
{
	return SAA716x_EPRD(dev, AI_CTL) == 0 ? 0 : -1;
}
EXPORT_SYMBOL_GPL(saa716x_aip_status);

void saa716x_aip_disable(struct saa716x_dev *saa716x)
{
	SAA716x_EPWR(AI0, AI_CTL, 0x00);
	SAA716x_EPWR(AI1, AI_CTL, 0x00);
}
EXPORT_SYMBOL_GPL(saa716x_aip_disable);
