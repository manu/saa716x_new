#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/pci.h>
#include <linux/signal.h>
#include <linux/sched.h>
#include <linux/interrupt.h>
#include <linux/scatterlist.h>
#include <asm/page.h>
#include <asm/pgtable.h>

#include <linux/i2c.h>

#include "dvbdev.h"
#include "dvb_demux.h"
#include "dmxdev.h"
#include "dvb_frontend.h"
#include "dvb_net.h"

#include "saa716x_mod.h"

#include "saa716x_i2c_reg.h"
#include "saa716x_msi_reg.h"
#include "saa716x_cgu_reg.h"
#include "saa716x_vip_reg.h"

#include "saa716x_cgu.h"
#include "saa716x_boot.h"
#include "saa716x_i2c.h"
#include "saa716x_msi.h"
#include "saa716x_spi.h"
#include "saa716x_dma.h"
#include "saa716x_fgpi.h"
#include "saa716x_priv.h"

void saa716x_vipint_disable(struct saa716x_dev *saa716x)
{
	SAA716x_EPWR(VI0, INT_ENABLE, 0); /* disable VI 0 IRQ */
	SAA716x_EPWR(VI1, INT_ENABLE, 0); /* disable VI 1 IRQ */
	SAA716x_EPWR(VI0, INT_CLR_STATUS, 0x3ff); /* clear IRQ */
	SAA716x_EPWR(VI1, INT_CLR_STATUS, 0x3ff); /* clear IRQ */
}
EXPORT_SYMBOL_GPL(saa716x_vipint_disable);

void saa716x_vip_disable(struct saa716x_dev *saa716x)
{
       SAA716x_EPWR(VI0, VIP_POWER_DOWN, VI_PWR_DWN);
       SAA716x_EPWR(VI1, VIP_POWER_DOWN, VI_PWR_DWN);
}
EXPORT_SYMBOL_GPL(saa716x_vip_disable);
