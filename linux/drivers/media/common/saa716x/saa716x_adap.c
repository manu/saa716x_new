#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/pci.h>
#include <linux/signal.h>
#include <linux/sched.h>
#include <linux/interrupt.h>
#include <linux/scatterlist.h>

#include <linux/i2c.h>
#include <linux/bitops.h>

#include "dvbdev.h"
#include "dvb_demux.h"
#include "dmxdev.h"
#include "dvb_frontend.h"
#include "dvb_net.h"
#include "dvb_ca_en50221.h"

#include "saa716x_mod.h"

#include "saa716x_fgpi_reg.h"
#include "saa716x_dma_reg.h"
#include "saa716x_msi_reg.h"
#include "saa716x_adap.h"

#include "saa716x_gpio.h"
#include "saa716x_cgu.h"
#include "saa716x_boot.h"
#include "saa716x_i2c.h"
#include "saa716x_msi.h"
#include "saa716x_spi.h"
#include "saa716x_dma.h"
#include "saa716x_fgpi.h"
#include "saa716x_priv.h"


DVB_DEFINE_MOD_OPT_ADAPTER_NR(adapter_nr);


//void saa716x_dma_start(struct saa716x_dev *saa716x, u8 adapter)
void saa716x_dma_start(struct saa716x_dev *saa716x, u8 stream)
{
	struct fgpi_stream_params params;
	int ret = 0;

//	dprintk(SAA716x_DEBUG, 1, "SAA716x Start DMA engine for Adapter:%d FGPI-%d",
//		adapter,
//		saa716x->config->stream_cfg[adapter].ts_port);

	dprintk(SAA716x_DEBUG, 1, "SAA716x Start DMA engine for Stream:%d", stream);


	params.bits		= 8;
	params.samples		= 188;
	params.lines		= 348;
	params.pitch		= 188;
	params.offset		= 0;
	params.page_tables	= 0;
	params.stream_type	= FGPI_TRANSPORT_STREAM;
	params.stream_flags	= 0;

//	ret = saa716x_fgpi_start(saa716x,
//				 saa716x->config->stream_cfg[adapter].ts_port,
//				 &params);
	ret = saa716x_fgpi_start(saa716x,
				 stream,
				 &params);
	if (ret)
		dprintk(SAA716x_ERROR, 1, "FGPI start failed, ret=%d", ret);
}

//void saa716x_dma_stop(struct saa716x_dev *saa716x, u8 adapter)
void saa716x_dma_stop(struct saa716x_dev *saa716x, u8 stream)
{
//	dprintk(SAA716x_DEBUG, 1, "SAA716x Stop DMA engine for Adapter:%d FGPI-%d",
//		adapter,
//		saa716x->config->stream_cfg[adapter].ts_port);
	dprintk(SAA716x_DEBUG, 1, "SAA716x Stop DMA engine for Stream:%d", stream);
//	saa716x_fgpi_stop(saa716x, saa716x->config->stream_cfg[adapter].ts_port);
	saa716x_fgpi_stop(saa716x, stream);
}

static int saa716x_dvb_start_feed(struct dvb_demux_feed *dvbdmxfeed)
{
	struct dvb_demux *dvbdmx	= dvbdmxfeed->demux;
	struct saa716x_dvb *dvb		= dvbdmx->priv;
	struct saa716x_dev *saa716x	= dvb->saa716x;
	struct saa716x_config *config	= saa716x->config;
//	u16 maxstr = 0;
	int mux_pos = 0;

//	dprintk(SAA716x_DEBUG, 1, "SAA716x DVB Start feed");
	if (!dvbdmx->dmx.frontend) {
		dprintk(SAA716x_DEBUG, 1, "no frontend ?");
		return -EINVAL;
	}
	if (config->st_devs > config->fe_devs) {
//		/* get max stream mux positions */
//		maxstr = config->st_devs / config->fe_devs;
		/* get current stream mux position */
		if (config->strmux_pos)
			config->strmux_pos(dvb, &mux_pos);
//		BUG_ON(mux_pos > maxstr);
	}
	dvb->feeds++;
	dprintk(SAA716x_DEBUG, 1, "SAA716x start feed, feeds=%d", dvb->feeds);

	if (dvb->feeds == 1) {
		dprintk(SAA716x_DEBUG, 1, "SAA716x start feed & dma");
		if (config->st_devs > config->fe_devs) {
			saa716x_dma_start(saa716x, mux_pos);
			/* save current MUX position */
			dvb->strmux_pos = mux_pos;
		} else {
			saa716x_dma_start(saa716x, dvb->count);
		}
	}
	return dvb->feeds;
}

static int saa716x_dvb_stop_feed(struct dvb_demux_feed *dvbdmxfeed)
{
	struct dvb_demux *dvbdmx = dvbdmxfeed->demux;
	struct saa716x_dvb *dvb		= dvbdmx->priv;
	struct saa716x_dev *saa716x	= dvb->saa716x;
	struct saa716x_config *config	= saa716x->config;

//	dprintk(SAA716x_DEBUG, 1, "SAA716x DVB Stop feed");
	if (!dvbdmx->dmx.frontend) {
		dprintk(SAA716x_DEBUG, 1, "no frontend ?");
		return -EINVAL;
	}
	dvb->feeds--;
	if (dvb->feeds == 0) {
		dprintk(SAA716x_DEBUG, 1, "saa716x stop feed and dma");
		if (config->st_devs > config->fe_devs)
			saa716x_dma_stop(saa716x, dvb->strmux_pos);
		else
			saa716x_dma_stop(saa716x, dvb->count);
	}
	return 0;
}

int saa716x_dvb_init(struct saa716x_dev *saa716x)
{
	struct saa716x_config *config = saa716x->config;
	struct saa716x_dvb *dvb;
	int result, i;

	mutex_init(&saa716x->adap_lock);

	dprintk(SAA716x_DEBUG, 1, "dvb_register_adapter");
	result = dvb_register_adapter(&saa716x->dvb_adapter,
				      "SAA716x dvb adapter",
				      THIS_MODULE,
				      &saa716x->pdev->dev,
				      adapter_nr);
	if (result < 0) {
		dprintk(SAA716x_ERROR, 1, "Error registering adapter");
		return -ENODEV;
	}

	dvb = kzalloc((sizeof (struct saa716x_dvb) * config->fe_devs), GFP_KERNEL);
	if (!dvb) {
		dprintk(SAA716x_ERROR, 1, "Out of memory!");
		result = -ENOMEM;
		goto err0;
	}
	saa716x->dvb = dvb;
	saa716x->dvb_adapter.priv = dvb;
	for (i = 0; i < config->fe_devs; i++) {
		dprintk(SAA716x_DEBUG, 1, "DVB:%d Init", i);
		BUG_ON(!dvb);
		dvb->count			= i;
		dvb->demux.dmx.capabilities	= DMX_TS_FILTERING	|
						  DMX_SECTION_FILTERING	|
						  DMX_MEMORY_BASED_FILTERING;

		dvb->demux.priv			= dvb;
		dvb->demux.filternum		= 256;
		dvb->demux.feednum		= 256;
		dvb->demux.start_feed		= saa716x_dvb_start_feed;
		dvb->demux.stop_feed		= saa716x_dvb_stop_feed;
		dvb->demux.write_to_decoder	= NULL;

		dprintk(SAA716x_DEBUG, 1, "dvb_dmx_init");
		result = dvb_dmx_init(&dvb->demux);
		if (result < 0) {
			dprintk(SAA716x_ERROR, 1, "dvb_dmx_init failed, ERROR=%d", result);
			goto err0;
		}

		dvb->dmxdev.filternum		= 256;
		dvb->dmxdev.demux		= &dvb->demux.dmx;
		dvb->dmxdev.capabilities	= 0;

		dprintk(SAA716x_DEBUG, 1, "dvb_dmxdev_init");
		result = dvb_dmxdev_init(&dvb->dmxdev, &saa716x->dvb_adapter);
		if (result < 0) {
			dprintk(SAA716x_ERROR, 1, "dvb_dmxdev_init failed, ERROR=%d", result);
			goto err1;
		}

		dvb->fe_hw.source = DMX_FRONTEND_0;

		result = dvb->demux.dmx.add_frontend(&dvb->demux.dmx, &dvb->fe_hw);
		if (result < 0) {
			dprintk(SAA716x_ERROR, 1, "dvb_dmx_init failed, ERROR=%d", result);
			goto err2;
		}

		dvb->fe_mem.source = DMX_MEMORY_FE;

		result = dvb->demux.dmx.add_frontend(&dvb->demux.dmx, &dvb->fe_mem);
		if (result < 0) {
			dprintk(SAA716x_ERROR, 1, "dvb_dmx_init failed, ERROR=%d", result);
			goto err3;
		}

		result = dvb->demux.dmx.connect_frontend(&dvb->demux.dmx, &dvb->fe_hw);
		if (result < 0) {
			dprintk(SAA716x_ERROR, 1, "dvb_dmx_init failed, ERROR=%d", result);
			goto err4;
		}

		dvb_net_init(&saa716x->dvb_adapter, &dvb->dvb_net, &dvb->demux.dmx);
		dprintk(SAA716x_DEBUG, 1, "Frontend Init");
		dvb->saa716x = saa716x;

		if (config->frontend_attach) {
			result = config->frontend_attach(dvb, i);
			if (result < 0)
				dprintk(SAA716x_ERROR, 1, "SAA716x frontend:%d attach failed", i);

			if (!dvb->fe) {
				dprintk(SAA716x_ERROR, 1, "A frontend driver was not found for [%04x:%04x] subsystem [%04x:%04x]\n",
					saa716x->pdev->vendor,
					saa716x->pdev->device,
					saa716x->pdev->subsystem_vendor,
					saa716x->pdev->subsystem_device);
			} else {
				result = dvb_register_frontend(&saa716x->dvb_adapter, dvb->fe);
				if (result < 0) {
					dprintk(SAA716x_ERROR, 1, "SAA716x register frontend failed");
					goto err6;
				}
			}

		} else {
			dprintk(SAA716x_ERROR, 1, "Frontend attach = NULL");
		}
#if 1
		if (config->ci_attach) {
			result = config->ci_attach(dvb, i);
			if (result < 0)
				dprintk(SAA716x_ERROR, 1, "SAA716x CI attach failed");
			if (!dvb->ci) {
				dprintk(SAA716x_ERROR, 1, "A CI driver was not found for [%04x:%04x] subsystem [%04x:%04x]\n",
					saa716x->pdev->vendor,
					saa716x->pdev->device,
					saa716x->pdev->subsystem_vendor,
					saa716x->pdev->subsystem_device);
			} else {
				dprintk(SAA716x_ERROR, 1, "Registering EN50221 device");
				result = dvb_ca_en50221_init(&saa716x->dvb_adapter,
							     dvb->ci,
							     dvb->ca_flags,
							     1);
				if (result) {
					dprintk(SAA716x_ERROR, 1, "EN50221: Initialization failed <%d>", result);
//					goto err;
				}
			}
		}
#endif
		dvb++;
	}
	for (i = 0; i < config->st_devs; i++) {
		result = saa716x_fgpi_init(saa716x, config->stream_cfg[i].ts_port);
		if (result < 0)
			dprintk(SAA716x_ERROR, 1, "FGPI Initialization error, err=%d", result);
	}
	return 0;

	/* Error conditions */
err6:
	dvb_frontend_detach(dvb->fe);
err4:
	dvb->demux.dmx.remove_frontend(&dvb->demux.dmx, &dvb->fe_mem);
err3:
	dvb->demux.dmx.remove_frontend(&dvb->demux.dmx, &dvb->fe_hw);
err2:
	dvb_dmxdev_release(&dvb->dmxdev);
err1:
	dvb_dmx_release(&dvb->demux);
err0:
	dvb_unregister_adapter(&saa716x->dvb_adapter);
	return result;
}
EXPORT_SYMBOL(saa716x_dvb_init);

void saa716x_dvb_exit(struct saa716x_dev *saa716x)
{
	struct saa716x_dvb *dvb = saa716x->dvb;
	int i;

	for (i = 0; i < saa716x->config->st_devs; i++)
		saa716x_fgpi_exit(saa716x, saa716x->config->stream_cfg[i].ts_port);

	for (i = 0; i < saa716x->config->fe_devs; i++) {

		BUG_ON(!dvb);

		if (dvb->fe) {
			dvb_unregister_frontend(dvb->fe);
			dvb_frontend_detach(dvb->fe);
		}

		dvb_net_release(&dvb->dvb_net);
		dvb->demux.dmx.remove_frontend(&dvb->demux.dmx, &dvb->fe_mem);
		dvb->demux.dmx.remove_frontend(&dvb->demux.dmx, &dvb->fe_hw);
		dvb_dmxdev_release(&dvb->dmxdev);
		dvb_dmx_release(&dvb->demux);

		dvb++;
	}
	dprintk(SAA716x_DEBUG, 1, "dvb_unregister_adapter");
	dvb_unregister_adapter(&saa716x->dvb_adapter);
	return;
}
EXPORT_SYMBOL(saa716x_dvb_exit);
